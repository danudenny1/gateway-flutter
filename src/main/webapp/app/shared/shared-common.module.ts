import { NgModule } from '@angular/core';

import { GatewayflutterSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [GatewayflutterSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [GatewayflutterSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class GatewayflutterSharedCommonModule {}
